const winston = require("winston");
const fetch = require("node-fetch");
const cookieParser = require("cookie-parser");
const csrf = require("csurf");
const bodyParser = require("body-parser");
const express = require("express");

const config = require("./config");

var csrfProtection = csrf({cookie: true});
var parseForm = bodyParser.urlencoded({extended: false});

var app = express();

// конфигурация сервера
app.set("port", (process.env.PORT || config.port));

app.set("view engine", "pug");

// middleware
app.use(cookieParser());
app.use(express.static("public"));

// роутинг
app.get("/", csrfProtection, function(req, res) {
    let csrfToken = req.csrfToken();
    winston.log("info", `Generated csrf token = ${csrfToken}`);
    res.render("index", { "csrfToken": csrfToken });
})

app.post("/send-request", parseForm, csrfProtection, (request, response) => {
    let state = `security_token=${request.csrfToken()}`;
    response.cookie("state", state);
    winston.log("info", `Generated state token for google request = ${state}`);
    let url = getCurrentHostUrl(request);
    let requestToGoogle = createRequestForGettingCode({
        baseUrl: config.baseUrl,
        clientId: config.clientId,
        scope: config.scope,
        state: state,
        redirectUri: `${url}${config.authCallback}`
    });

    winston.log("info", `Created request to Google = ${requestToGoogle}`);
    fetch(requestToGoogle)
        .then(res => {
            return res.text();
        }).then(body => {
            response.send(body);
            winston.log("info", "end of 1 request to google");
        });

})

function createRequestForGettingCode(params) {
    return `${params.baseUrl}?` +
        `client_id=${params.clientId}&` +
        `response_type=code&` +
        `scope=${params.scope}&` +
        `state=${params.state}&` +
        `redirect_uri=${params.redirectUri}`;
}

app.get(config.authCallback, (request, response) => {
    winston.log("info", `request.body = "${JSON.stringify(request.body)}"`);
    let savedState = request.cookies.state;
    let state = request.query.state;
    if (state && savedState == state) {
        let code = request.query.code;
        winston.log("info", `code = ${code}`);
        let hostUrl = getCurrentHostUrl(request);
        winston.log("info", `host url = ${hostUrl}`);
        let postReqBody = createReqBodyForAccessTokenAndId({
            code: code,
            clientId: config.clientId,
            clientSecret: config.clientSecret,
            redirectUri: `${hostUrl}${config.authCallback}`,
            grantType: `authorization_code`
        });
        winston.log("info", `Generated request for access token: "${postReqBody}"`);
        fetch(config.urlV4Token, {method: "POST",
            body: postReqBody,
            headers: {"Content-Type": "application/x-www-form-urlencoded"}
        })
            .then(resp => {
                return resp.json();
            }).then(json => {

                response.cookie("googleJson", json);
                response.redirect("/cabinet");
            });
    }

    else
        response.status(401).send(`Invalid state parameter.`);
});

app.get("/cabinet", (request,  response) => {
    let googleJson = request.cookies.googleJson;
    let id_token = googleJson.id_token;
    winston.log("info", `encoded id_token = ${id_token}`);
    let [header, payload, signature] = decode(id_token);
    winston.log("info", `decoded header from id_token = "${JSON.stringify(header)}"`);
    winston.log("info", `decoded payload from id_token = "${JSON.stringify(payload)}"`);
    winston.log("info", `signature from id_token = "${signature}"`);
    let access_token = googleJson.access_token;
    winston.log("info", `access_token = ${access_token}`);
    fetch("https://www.googleapis.com/drive/v2/files", {
        method: "GET",
        headers: {"Authorization": `Bearer ${access_token}`}
    }).then(resp => {
        return resp.json();
    }).then(json => {
        response.render("cabinet", {email: payload.email, picture: payload.picture, files: json.items});
    })
});

function decode(jwt) {
    let encodedArr = jwt.split(".");
    let header = JSON.parse(Buffer.from(encodedArr[0], "base64").toString("utf-8"));
    let payload = JSON.parse(Buffer.from(encodedArr[1], "base64").toString("utf-8"));
    let signature = encodedArr[2];
    return [header, payload, signature];
}


function createReqBodyForAccessTokenAndId(params) {
    return `code=${params.code}&` +
        `client_id=${params.clientId}&` +
        `client_secret=${params.clientSecret}&` +
        `redirect_uri=${params.redirectUri}&` +
        `grant_type=${params.grantType}`;
}


function getCurrentHostUrl(request) {
    let protocol = "https"; 
    let host = request.get("host");
    let originalUrl = request.originalUrl;
    return `${protocol}://${host}`;
}


// запуск сервера
var port = app.get("port");
app.listen(port, () => {
    winston.log("info", `ExpressJs application is listening on port ${port}`);
});

